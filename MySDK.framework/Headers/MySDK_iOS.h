//
//  MySDK_iOS.h
//  MySDK iOS
//
//  Created by Lukáš Fečke on 14/03/2019.
//

#import <UIKit/UIKit.h>

//! Project version number for MySDK_iOS.
FOUNDATION_EXPORT double MySDK_iOSVersionNumber;

//! Project version string for MySDK_iOS.
FOUNDATION_EXPORT const unsigned char MySDK_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MySDK_iOS/PublicHeader.h>


